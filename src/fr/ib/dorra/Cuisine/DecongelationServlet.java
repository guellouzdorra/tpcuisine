package fr.ib.dorra.Cuisine;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DecongelationServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		 req.getRequestDispatcher("/WEB-INF/cacher/decongelation.jsp").forward(req, resp);
		 

	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		
		
		
		 if (req.getParameter("masse")==null || req.getParameter("masse").trim().equals("")) {
	        	req.setAttribute("message", "masse manquante");
	        	req.setAttribute("masse", req.getParameter("masse"));
	        	doGet(req,resp);
	        	
		  }else {int masse = Integer.parseInt(req.getParameter("masse"));
              req.setAttribute("temps_heures", masse*10);
              req.setAttribute("temps_min", masse*12);


            req.getRequestDispatcher("/WEB-INF/cacher/tempsDecongelation.jsp").forward(req, resp);

       }
		 
		
		 
			
			}
}
		




