package fr.ib.dorra.Cuisine;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ConversionServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] unites = {"grammes","kilogrammes","litres","pincées",
				"cuillères à café", "cuillères à soupe", "tasses"};
		req.setAttribute("unites", unites);
		req.getRequestDispatcher("/WEB-INF/cacher/conversion.jsp").forward(req,resp);
		
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        float quantite = Integer.parseInt(req.getParameter("quantité"));
        
        String unite2 = req.getParameter("unite");
        req.setAttribute("unite", unite2);
        
        
        if (unite2.equals("grammes")) {
        	float conversion[] = new float [] {quantite/1000, quantite/1000, quantite/2, quantite/7, quantite/15, quantite/115};
        	String unite[] = new String[] {"kilogrammes", "litres", "pincées", "cuillères à café", "cuillères à soupe", "tasses"};
			req.setAttribute("Conv", conversion);
			req.setAttribute("Unit", unite);
        } else if (unite2.equals("kilogrammes")) {
        	float conversion[] = new float [] {quantite*1000, quantite*1, quantite*500 , quantite*143, quantite*67, quantite*9};
        	String unite[] = new String[] {"grammes", "litres", "pincées", "cuillères à café", "cuillères à soupe", "tasses"};
			req.setAttribute("Conv", conversion);
			req.setAttribute("Unit", unite);
        } else if (unite2.equals("litres")) {
        	String unite[] = new String[] {"grammes", "kilogrammes", "pincées", "cuillères à café", "cuillères à soupe", "tasses"};
        	float conversion[] = new float [] {quantite*1000, quantite*1, quantite*500, quantite*143, quantite*67, quantite*9};
			req.setAttribute("Conv", conversion);
			req.setAttribute("Unit", unite);
        } else if (unite2.equals("pincées")) {
        	String unite[] = new String[] {"grammes", "kilogrammes", "litres", "cuillères à café", "cuillères à soupe", "tasses"};
        	float conversion[] = new float [] {quantite*2, quantite/500, quantite/500, (quantite/7)*2, (quantite/15)*2, (quantite/115)*2};
			req.setAttribute("Conv", conversion);
			req.setAttribute("Unit", unite);
        } else if (unite2.equals("cuillères à café")) {
        	String unite[] = new String[] {"grammes", "kilogrammes", "litres", "pincées", "cuillères à soupe", "tasses"};
        	float conversion[] = new float [] {quantite*7, (quantite/1000)*7, (quantite/1000)*7, (quantite/2)*7, (quantite/15)*7, (quantite/115)*7};
			req.setAttribute("Conv", conversion);
			req.setAttribute("Unit", unite);
        } else if (unite2.equals("cuillères à soupe")) {
        	String unite[] = new String[] {"grammes", "kilogrammes", "litres","pincées", "cuillères à café", "tasses"};
        	float conversion[] = new float [] {quantite*15, (quantite/1000)*15, (quantite/1000)*15, (quantite/2)*15, (quantite/7)*15, (quantite/115)*15};
			req.setAttribute("Conv", conversion);
			req.setAttribute("Unit", unite);
        } else  {
        	String unite[] = new String[] {"grammes", "kilogrammes", "litres","pincées", "cuillères à café", "cuillères à soupe"};
        	float conversion[] = new float [] {quantite*115, (quantite/1000)*115, (quantite/1000)*115, (quantite/2)*115, (quantite/7)*115, (quantite/15)*115};
			req.setAttribute("Conv", conversion);
			req.setAttribute("Unit", unite);
        }
        
        req.getRequestDispatcher("/WEB-INF/cacher/conversion-succes.jsp").forward(req,resp);
        	
        
	}
}



